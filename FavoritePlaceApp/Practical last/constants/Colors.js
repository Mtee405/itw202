export const Colors= {
    primary50: '#cfeffd',
    primary100:'#a0defb',
    primary200:'#77cff8',
    primary400:'#44bdf5',
    primary500:'#07a8f2',
    primary700:'#0570c9',
    primary800:'#003b88',
    primary500:'e6b30b',
    gray700:'#221c30',
};