import { StyleSheet, View, Image, Text } from 'react-native'
import React from 'react'
import Title from '../component/Title'
import Colors from '../constants/Color'
import PrimaryButton from '../component/PrimaryButton'
import { Dimensions } from 'react-native'
import { useWindowDimensions } from 'react-native'

function GameOverScreen({roundsNumber, userNumber, onStartNewGame}) {
    const {width, height} = useWindowDimensions();

    let imageSize = 300;
    if (width < 300){
        imageSize = 150;
    }

    if (height < 400){
        imageSize = 80;
    }

    const imageStyle = {
        width: imageSize,
        height: imageSize,
        borderRadius: imageSize / 2
    }
  return (
    <View style={styles.rootContainer}>
        <Title>GAME OVER!</Title>
        <View style={[styles.imageContainer, imageStyle]}>
        <Image 
            style={styles.image}
            source={require('../assets/success.png')}/>
        </View>
        <Text style={styles.summaryText}> 
                Your Phone needed <Text style={styles.highlight}>{roundsNumber}</Text> rounds to guess the number 
                <Text style={styles.highlight}> {userNumber} </Text>
        </Text>
        <PrimaryButton onPress={onStartNewGame}> Start New Game</PrimaryButton>
    </View>
  )
}

export default GameOverScreen
const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    rootContainer:{
        flex: 1,
        padding: 24,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imageContainer:{
        width: deviceWidth < 380 ? 150: 300,
        height: deviceWidth < 380 ? 150: 300,
        borderRadius: deviceWidth < 380 ? 75: 150,
        borderWidth: 3,
        borderColor: Colors.primary800,
        overflow: 'hidden',
        margin: 36,
    },
    image:{
        height: '100%',
        width: '100%'
    },
    summaryText:{
        // fontFamily: 'open-sans',
        fontSize: 24,
        textAlign: 'center',
        marginBottom: 24,
    },
    highlight:{
        // fontFamily: 'open-sans-bold',
        color: Colors.primary500
    }
    

    
})