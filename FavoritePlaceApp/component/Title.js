import { StyleSheet, Text, View } from 'react-native'
function Title({children}){
  return(
    <Text style={styles.title}>{children}</Text>
  )
}
// import React from 'react'
// import Colors from '../constants/Color'

// const Title = ({children}) => {
//   return (

//     <Text style={styles.title}>{children}</Text>
//   )
// }
export default Title;

const styles = StyleSheet.create({
    title: {
      //fontFamily: 'open-sans-bold',
      fontSize: 24,
      color: 'white',
      textAlign: 'center',
      borderWidth: 2,
      borderColor: 'white',
      padding:12,
      maxWidth: '80%',
      width: 300,
        // borderWidth:3,
        // borderColor: Colors.accent500,
        // textAlign: 'center',
        // color: 'white',
        // fontSize: 24,
        // fontWeight:'bold',
        
      },
})