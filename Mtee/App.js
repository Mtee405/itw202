import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { add, multiply } from './Component/nameExport';
import division from './Component/defaultExport';
import divide from './Component/defaultExport';
export default function App() {
  return (
    <View style={styles.container}>
      <Text>ITW202 is our module to work on MA</Text>
      <Text>Result of addition: {add(5, 6)} </Text>
      <Text> Result of multiplication: {multiply(5,8)}</Text>
      <Text>Result of division: {divide(10,2)}</Text>
      <StatusBar style="auto" />

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

