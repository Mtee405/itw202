import { View, Text, Image, StyleSheet } from 'react-native'
import React from 'react'
import Title from '../components/ui/Title'
import colors from '../constants/Colors';
import PrimaryButton from '../components/ui/PrimaryButton';

export default function GameOverScreen({roundNumber, userNumber, onStartNewGame}) {
  return (
    <View style={styles.rootcontainer}>
      <Title>GAME OVER</Title>
      <View style={styles.Imagecontainer}>
      <Image style={styles.image} source={require('../assets/images/success.png')}/>
      </View>
      <Text style={styles.summaryText}>Your phone needed <Text style={styles.heighlight}>{roundNumber}</Text> rounds to guess the number <Text style={styles.heighlight}>{userNumber}</Text></Text>
      <PrimaryButton onPress={onStartNewGame}>Start New Game</PrimaryButton>    
    </View>
  )
}

const styles = StyleSheet.create({
  rootcontainer:{
    flex:1,
    padding:24,
    alignItems:'center',
    justifyContent:'center'
  },

  Imagecontainer:{ 
    width:300, 
    height:300, 
    borderRadius:150,
    borderWidth:3,
    borderColor:colors.primary800,
    overflow:'hidden',
    margin:36
  },

  image:{
    height:'100%',
    width:'100%'
  },

  summaryText:{
    fontFamily:'open-sans',
    fontSize:24,
    textAlign:'center', 
   },

   heighlight:{
     fontFamily:'open-sans-bold',
     color:colors.primary500
   }
});