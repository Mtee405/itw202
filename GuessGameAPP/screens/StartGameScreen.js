import { View, Text,TextInput, StyleSheet, Alert, onPickNumber } from 'react-native'
import React, {useState, useEffect}from 'react'
import PrimaryButton from '../components/ui/PrimaryButton';
import colors from '../constants/Colors';
import Title from '../components/ui/Title';
import Card from '../components/ui/Card';
import InstructionText from '../components/ui/InstructionText';

export default function StartGameScreen({onPickNumber}) {
  const[enterNumber, setEnterNumber]=useState('');

  function numberInputHandler(enterText){
    setEnterNumber(enterText);
  }
  function confirmInputhandler(){
    const chosenNumber=parseInt(enterNumber)

    if(isNaN(chosenNumber) || chosenNumber < 1 || chosenNumber > 99){
      Alert.alert('Invalid Input value', 'Number shoud be between 1 to 99',
      [{text:"Ok", style:"destructive", onPress:resetInputHandler}])
      return;
    }
    // console.log(chosenNumber)
    onPickNumber(chosenNumber)
    
  }
  function resetInputHandler(){
    setEnterNumber('');
  }
  
  return (
    <View style={styles.rootContainer}>
      <Title>Guess My  Number</Title>
      <Card style={styles.inputcontainer}>
      <InstructionText>Enter a Number</InstructionText>
        <TextInput style={styles.numberInput}
            keyboardType='number-pad'
            autoCorrect={false}
            autoCapitalize='none'
            maxLength={2} 
            value={enterNumber}
            onChangeText={numberInputHandler}/>
        <View style={styles.buttonsContainer}>
           <View style={styles.buttonContainer}>
              <PrimaryButton onPress={resetInputHandler}>Reset</PrimaryButton>
           </View>
           <View style={styles.buttonContainer}>
              <PrimaryButton onPress={confirmInputhandler}>Confirm</PrimaryButton>
           </View>
        </View>
      </Card>  
    </View>   
  )
}
const styles = StyleSheet.create({
  rootContainer:{
    flex: 1,
    marginTop: 100,
    alignItems: 'center'
  },
  instructionText: {
    color:colors.accent500,
    fontSize:24
  },
    inputcontainer: {
        justifyContent: 'center',
        alignItems:'center',
        marginTop: 100,
        marginHorizontal: 24,
        padding: 16,
        backgroundColor: '#72063C',
        borderRadius: 8,
        elevation: 4,
        //   for IOS
    //   shadowColor:'black',
    //   shadowOffset:{width:0, height:2},
    //   shadowRadius:5,
    //   shadowOpacity:0.25,
      },
      numberInput: {
        height: 50,
        width: 50,
        fontSize: 32,
        borderBottomColor:'#DDB52F',
        borderBottomWidth: 2,
        color: '#DDB52F',
        marginVertical: 8,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    buttonsContainer: {
        flexDirection: 'row'
    },
    buttonContainer: {
        flex: 1,
    }
  });