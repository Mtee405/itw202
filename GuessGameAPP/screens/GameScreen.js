import { StyleSheet, Text, View, Alert } from 'react-native'
import React, {useState, useEffect} from 'react'
import Title from '../components/ui/Title'
import NumberContainer from '../components/Game/NumberContainer';
import PrimaryButton from '../components/ui/PrimaryButton';
import InstructionText from '../components/ui/InstructionText';
import Card from '../components/ui/Card';
import {Ionicons} from '@expo/vector-icons'
import GuessLogItem from '../components/Game/GuessLogItem';
import { FlatList } from 'react-native';

function generateRandomNumber(min, max, exclude){
  const rndNum=Math.floor(Math.random()*(max-min))+min;
  if(rndNum==exclude){
    return generateRandomNumber(min, max, exclude) 
  }
  else{
    return rndNum;
  }
}

let minBoundary=1;
let maxBoundary=100;

const GameScreen = ({userNumber, onGameOver}) => {
  const initialGuess = generateRandomNumber(1, 100, userNumber)
  const [currentGuess, setCurrentGuess]=useState(initialGuess)
  const [guessRounds, setGuessRounds]=useState([initialGuess]);

useEffect(()=>{
  if(currentGuess=== userNumber){
    onGameOver(guessRounds.length);
  }
},[currentGuess, userNumber, onGameOver])

useEffect(()=>{
  minBoundary=1;
  maxBoundary=100;
},[])

  function nextGuessHandler(direction){
    if(
      (direction==='lower' && currentGuess < userNumber) ||
      (direction==='greater' && currentGuess > userNumber)
    ){
      Alert.alert("Dont lie!", "You know that this wrong...",[
        {text: "Sorry", style: "cancel"}

      ])
      return;
    }
    if(direction=='lower'){
      maxBoundary = currentGuess;
    }
    else{
      minBoundary = currentGuess+1;
    }
    console.log(minBoundary,maxBoundary)
    const newRndNumber = generateRandomNumber(minBoundary, maxBoundary, currentGuess)
    setCurrentGuess(newRndNumber);
    setGuessRounds((prevGuessRounds =>[newRndNumber, ...prevGuessRounds]))
  }
  const guessRoundsListLength = guessRounds.length

  return (
    <View style={styles.screen}>
    <Title>Opponent's Guess</Title>
    <NumberContainer>{currentGuess}</NumberContainer>
      <Card>
        <InstructionText style={styles.instructionText}>Higher or Lower?</InstructionText>
        <View style={styles.buttonsContainer}>
        <View style={styles.buttonContainer}>
          <PrimaryButton onPress={nextGuessHandler.bind(this, 'greater')}>
            <Ionicons name='md-add' size={24} color='white'/>
          </PrimaryButton>
        </View>
        <View style={styles.buttonContainer}>
          <PrimaryButton onPress={nextGuessHandler.bind(this, 'lower')}>
          <Ionicons name='md-remove' size={24} color='white'/>
          </PrimaryButton>
        </View>
        </View>
      </Card>
      {/* <View>
        {guessRounds.map(guessRound=><Text key={guessRound}>{guessRound}</Text>)}
      </View> */}
      <View style={styles.listcontainer}>
      <FlatList
        data={guessRounds}
        renderItem={(itemData)=> 
        <GuessLogItem
        roundNumber={guessRoundsListLength - itemData.index}
        guess = {itemData.item}
        />
        }
        keyExtractor={(item)=> item}
       />
        </View>
    </View>
  )
}

export default GameScreen

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 24,
  },
  buttonsContainer: {
    flexDirection:'row'
  },
  buttonContainer:{
    flex: 1,
  },
  instructionText:{
    marginBottom: 12
  },
  listcontainer:{
    flex:1,
    padding: 12,

  }
  
})