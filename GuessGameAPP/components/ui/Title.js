import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import colors from '../../constants/Colors'

const Title = ({children}) => {
  return (
    <Text style={styles.title}>{children}</Text>
  )
}

export default Title

const styles = StyleSheet.create({
    title:{
        borderWidth:2,
        borderColor:'white',
        textAlign:'center',
        color:'white',
        fontSize:24,
        fontWeight:'bold'
      }
})