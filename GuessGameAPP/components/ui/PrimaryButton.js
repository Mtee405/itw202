import { StyleSheet, Text, View, Pressable } from 'react-native'
import React from 'react'
// import Pressable from 'react-native/Libraries/Components/Pressable/Pressable'

const PrimaryButton = ({children, onPress}) => {
  return (
    <View style={styles.buttonOuterContainer}>
        <Pressable 
            android_ripple={{color:'yellow'}}
            style= {({pressed})=> pressed?
            [styles.buttonInnerContainer, styles.pressed ]:
            styles.buttonInnerContainer}
            onPress={onPress}>

            <Text style={styles.buttonText}>{children}</Text>

        </Pressable>
    </View>
      
    
  )
}

export default PrimaryButton

const styles = StyleSheet.create({
    buttonOuterContainer:{
        borderRadius:28,
        margin:4,
        overflow:'hidden'
    },
    buttonInnerContainer:{
        backgroundColor:'#72063c',
        borderRadius:28,
        paddingVertical:8,
        paddingHorizontal:16,
        elevation:4,
    },
    buttonText:{
        textAlign:'center',
        color:'white',
    },
    pressed:{
        opacity:0.75,
    }
})