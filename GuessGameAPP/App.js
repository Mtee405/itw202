import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, ImageBackground, SafeAreaView } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import StartGameScreen from './screens/StartGameScreen';
import GameScreen from './screens/GameScreen'
import { useState } from 'react';
import colors from './constants/Colors';
import GameOverScreen from './screens/GameOverScreen';
import { useFonts } from '@expo-google-fonts/inter';
// import Apploading from 'expo-app-loading'

export default function App() {
  const [userNumber, setUserNumber]=useState();
  const [gameIsOver, setGameIsOver] = useState(true);
  const [guessRounds, setGuessRounds] = useState(0)
  // const [guessRound, setGuessRound] = useState(0);

  const [fontsloaded]=useFonts({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf'),
  })
  
  // if(!fontsloaded){
  //  return <Apploading/>
  // }

  function pickedNumberHandler(pickerNumber){
    setUserNumber(pickerNumber)
    setGameIsOver(false);
  }
  function gameOverHandler(numberOfRounds){
    setGameIsOver(true);
    setGuessRounds(numberOfRounds)
  }
  function startNewGameHandler(){
    setUserNumber(null);
    setGuessRounds(0);
  }

  let screen = <StartGameScreen onPickNumber={pickedNumberHandler}/>
  if(userNumber){
    screen=<GameScreen userNumber={userNumber} onGameOver = {gameOverHandler}/>
  }
  if(gameIsOver && userNumber){
    screen = <GameOverScreen
                userNumber={userNumber}
                roundNumber={guessRounds}
                onStartNewGame={startNewGameHandler}
                />
  }
  return (
    <LinearGradient colors={['#4e0329','#ddb52f']} style={styles.container}>
      <ImageBackground source={require('./assets/images/background.png')} resizeMode='cover'
        style={styles.backgroundImage}/>
      <StatusBar style="auto" />
      <SafeAreaView style={styles.container}>
      {screen}
      {/* <GameOverScreen/> */}
      </SafeAreaView>
      
    </LinearGradient >
  );
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    //backgroundColor:'green'
  },
  backgroundImage:{
    position:'absolute', 
    width:'100%', 
    height:'100%', 
    opacity:0.4 

  }
});
