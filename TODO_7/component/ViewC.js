import React from 'react';
import { View,StyleSheet} from 'react-native';

export default function TD_7() {
  return (
    <View style={styles.container}>
        <View style={styles.v1}></View>
        <View  style={styles.v2}></View>   
    </View>
  );
}
const styles=StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        justifyContent:'flex-end',
        marginBottom:50

    },
    v1:{
        width:100,
        height:100,
        backgroundColor:'red'

    },
    v2:{
        width:100,
        height:100,
        backgroundColor:'blue'
    }

})
