import React,{useState} from 'react';
import { View, Text, TextInput,StyleSheet, Button } from 'react-native';

export default function TD_10() {
    const [text, setText] = useState('');
        
        
        return(

            <View style={styles.container}>
                <Text style={{marginBottom:100}}>Hi {text}</Text>
              
                <Text>what is your name?</Text>
                <TextInput secureTextEntry style={styles.input}
             onChangeText={text=>setText(text)}/>
                
            </View>
        )
}
const styles=StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',

        
    },
    input:{
       
        width:200,
        backgroundColor:'#dcdcdc',
        
    }
})
