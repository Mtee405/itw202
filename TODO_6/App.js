import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';


export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.B1}><Text>1</Text></View>
      <View style={styles.B2}><Text>2</Text></View>
      <View style={styles.B3}><Text>3</Text></View>
    </View>
  );}
const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "center",
    flexDirection: "row",
    padding: 50,
    paddingRight: 90,

    
  },
  B1: {
    backgroundColor: "red",
    width: 50,
    height: 300,
    justifyContent: "center",
    alignItems: "center"
    
  },
  B2: {
    backgroundColor: "blue",
    width: 150,
    height: 300,
    justifyContent: "center",
    alignItems: "center"
  },
  B3: {
    backgroundColor: "green",
    width: 10,
    height: 300,
    justifyContent: "center",
    alignItems: "center"
  },
});
