// import React from"react";
// import {View, StyleSheet, Text} from"react-native";

// const MyComponent = ()=>{
//     return(
//         <View>
//       <Text style = { StyleSheet.firstText}>Getting started with react native!</Text>
    
//         </View>
//     )
// };
// const style = StyleSheet.create({
//     firstText: {
//         fontsize:45,
//     }
// })
// export default MyComponent;
import React, { useState } from "react";
import { Text, StyleSheet } from "react-native";

const MyComponent = () => {
    const yourname='PwDendup';
  return (
  
    <Text style={styles.firstText}> Getting started {"\n"} with react {"\n"}native!
    {"\n"}
    <Text style={styles.secondText}>My name is {yourname}</Text>
    </Text>
    
  );
};

const styles = StyleSheet.create({
  firstText: {
    fontSize: 45,
  },
  secondText:{
      fontSize:20,
      
  } 
});

export default MyComponent;