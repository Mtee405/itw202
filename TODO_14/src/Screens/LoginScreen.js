import React, { useState } from "react";
import { View, StyleSheet,Text } from "react-native"
import Button from "../componenets/Button";
import Header from "../componenets/Header";
import Paragraph from "../componenets/Paragraph";
import Background from "../componenets/Background";
import Logo from "../componenets/Logo";
import TextInput from "../componenets/TextInput";
import { emailValidator } from "../core/helpers/emailValidator";
import { passwordValidator } from "../core/helpers/passwordValidator";
import BackButton from "../componenets/BackButton";
import { TouchableOpacity } from "react-native-gesture-handler";
import { theme } from "../core/theme";
import { LoginUser } from "../api/auth-api";


export default function LoginScreen({navigation}){
    const [email, setEmail]= useState({value:"",error:""})
    const [password, setPassword] = useState({ value:"" ,error:""})
    const [loading, setLoading]= useState();

    const onLoginPressed = async() =>{
        const emailError = emailValidator(email.value);
        const passwordError= passwordValidator(password.value);
        if(emailError || passwordError){
            setEmail({...email, error:emailError});
            setPassword({ ...password, error: passwordError});
        }
        setLoading(true)
        const response = await LoginUser({
            email: email.value,
            password: password.value,
        });
        if (response.error){
            alert(response.error);
        } 
        

        else{
            navigation.replace('HomeScreen')
        }
        setLoading(false)
    }
    return(
        <Background>
            <BackButton goBack={navigation.goBack} />
            <Logo/>
            <Header>Welcome</Header>
            <TextInput
                label="Email"
                value={email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={(text)=>setEmail({ value: text, error:""})}
            ></TextInput>
            <TextInput
                value={password.value}
                error={password.error}
                errorText = {password.error}
                onChangeText={(text)=>setPassword({ value:text, error: ""})}
                label="Password"
                secureTextEntry
            ></TextInput>
            <Button mode={"contained"} onPress ={onLoginPressed} >Login</Button>
           <View style={styles.forgotPassword}>
               <TouchableOpacity onPress={()=> navigation.navigate('ResetPasswordScreen')}>
                   <Text style={styles.forgot} > Forgot Your Password</Text>
               </TouchableOpacity>
           </View>

            <View style ={styles.row}>
                <Text>Dont have account?</Text>
                <TouchableOpacity onPress={()=> navigation.replace("RegisterScreen")}>
                <Text style={styles.link}>Sign Up</Text>
                </TouchableOpacity>
            </View>
        </Background>
    )
}

const styles = StyleSheet.create({
    row:{
        flexDirection:'row',
        marginTop:4,
    },
    link:{
        fontWeight:'bold',
        color:theme.colors.primary,
    },
    forgotPassword:{
        width:'100%',
        alignItems:'flex-end',
        marginBottom:24,
    },
    forgot:{
        fontSize:13,
        color:theme.colors.secondary,
    },


})