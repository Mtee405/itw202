import React from "react";
import { View, StyleSheet } from "react-native"
import Button from "../componenets/Button";
import Header from "../componenets/Header";
import { Paragraph } from "react-native-paper";
import Background from "../componenets/Background";
import Logo from "../componenets/Logo";

export default function StartScreen({navigation}){
    return(
        <Background>
            <Logo/>
            <Header>Login Template</Header>
            <Paragraph>
                The easiest way to start with your amazing application.
            </Paragraph>
            <Button
             mode="outlined"
             onPress = {()=>{
                 navigation.navigate("LoginScreen")
             }}
             >
                 Login
             </Button>
             <Button
             mode="contained"
             onPress ={()=>{
                 navigation.navigate("RegisterScreen")
             }}
             >
                 Sign Up
             </Button>
        </Background>
    )
}

// const styles = StyleSheet.create({
//     container: {
//         flex:1,
//         backgroundColor: "#fff",
//         alignItems: "center",
//         justifyContent: "center",
//         width: "100%",
    
//     },
// }); 