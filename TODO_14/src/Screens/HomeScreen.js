import React from "react";
import Header from "../componenets/Header";
import Background from "../componenets/Background";
import Button from "../componenets/Button";
import { LogoutUser } from "../api/auth-api";

export default function HomeScreen() {
    return (
        <Background> 
            <Header>Home</Header>
            <Button mode="contained"
            onPress={()=> {
                LogoutUser();
            }}>LogOut</Button>
        </Background>
    )
}
