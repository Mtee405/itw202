import React, { useState } from "react";
import { View, StyleSheet,Text } from "react-native"
import Button from "../componenets/Button";
import Header from "../componenets/Header";
import Paragraph from "../componenets/Paragraph";
import Background from "../componenets/Background";
import Logo from "../componenets/Logo";
import TextInput from "../componenets/TextInput";
import { emailValidator } from "../core/helpers/emailValidator";
import { passwordValidator } from "../core/helpers/passwordValidator";
import BackButton from "../componenets/BackButton";
import { TouchableOpacity } from "react-native-gesture-handler";
import { theme } from "../core/theme";


export default function ResetPasswordScreen({navigation}){
    const [email, setEmail]=useState({value:"",error:""})
    
    const onSubmitPressed = () =>{
        const emailError = emailValidator(email.value);
        
        if(emailError){
            setEmail({...email, error:emailError});
        }
    }
    return(
        <Background>
            <BackButton goBack={navigation.goBack} />
            <Logo/>
            <Header>Restore Password</Header>
            <TextInput
                label="Email"
                value={email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={(text)=>setEmail({ value: text, error:""})}
                description="You will recieve email with password reset link."
            ></TextInput>
            <Button mode={"contained"} onPress ={onSubmitPressed} >Send Instructions</Button>
        </Background>
    )
}
