import { StyleSheet, Text, View, TouchableOpacity, ScrollView, TouchableHighlight } from 'react-native'
import React from 'react'

const MoreOption = (props) => {
  return (
    <View style={styles.container}>
        <ScrollView>
            <TouchableOpacity style={styles.ListItem}> 
                <View style={styles.ListItemInnerContentView}>
                    <Text style={styles.Textstyle}>My Profile</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.ListItem}> 
                <View style={styles.ListItemInnerContentView}>
                    <Text style={styles.Textstyle}>about us</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.ListItem}
            onPress={() => props.navigation.replace('LoginScreen')}> 
                <View style={styles.ListItemInnerContentView}>
                    <Text style={styles.Textstyle}>LogOut</Text>
                </View>
            </TouchableOpacity>
        </ScrollView>
    </View>
  )
}

export default MoreOption

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor: 'white',
        padding:10,

    },
    ListItem:{
        backgroundColor:'#f6f6f6ff',
        width: '100%',
        height: 50,
        paddingHorizontal: 15,
    },
    ListItemInnerContentView:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        // alignItems:' center',
    },
    Textstyle:{
        fontSize: 15,
        color:'#676767ff',
        fontWeight:'400',

    },
})