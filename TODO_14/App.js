import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image } from 'react-native';
import { Provider } from 'react-native-paper';
import { theme } from './src/core/theme';
import Button from './src/componenets/Button';
import TextInput from './src/componenets/TextInput';
import Header from './src/componenets/Header';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { StartScreen, LoginScreen, RegisterScreen, ResetPasswordScreen, ProfileScreen, HomeScreen, AuthLoadingScreen } from './src/Screens';
import DrawerContent from './src/componenets/DrawerContent';
import firebase from 'firebase/compat/app';
import {firebaseConfig} from './src/core/FBConfig';
import MoreOption from './src/Screens/MoreOption';
import { TouchableOpacity } from 'react-native-gesture-handler';

if (!firebase.apps.length){
  firebase.initializeApp(firebaseConfig);
}

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <Provider theme={theme}>
    {/* <View style={styles.container}>

      <Text>Open</Text>
      <Button mode='contained'>Click Me</Button>
      <TextInput label="Email"/>
    </View> */}
    <NavigationContainer>
      <Stack.Navigator
      initialRouteName='StartScreen'
      screenOptions={{headerShown:false}}
      >
        <Stack.Screen name='AuthLoadingScreen' component={AuthLoadingScreen}/>
        <Stack.Screen name='StartScreen' component={StartScreen}/>
        <Stack.Screen name='LoginScreen' component={LoginScreen}/>
        <Stack.Screen name='RegisterScreen' component={RegisterScreen}/>
        <Stack.Screen name='ResetPasswordScreen' component={ResetPasswordScreen}/>
        {/* <Stack.Screen name='HomeScreen' component={BottomNavigation}/> */}
        <Stack.Screen name='HomeScreen' component={DrawerNavigator}/>
        <Stack.Screen name='MoreOption' component={MoreOption}/>

        
      </Stack.Navigator>
    </NavigationContainer>
    </Provider>
  );
}
function BottomNavigation() {
  return (
    <Tab.Navigator>
      <Tab.Screen 
      name='Home' 
      component={HomeScreen}
      options={{
        tabBarIcon: ({size}) => {
          return (
            <Image
            style={{ width: size, height: size }}
            source={require('./assets/Home.png')} />
          );
        },
      }}
      />
      <Tab.Screen 
        name='Profile' 
        component={ProfileScreen} 
        options={{
          tabBarIcon: ({size}) => {
            return (
              <Image
                style={{ width:size, height: size }}
                source={require('./assets/Profile.png')} />
            );
          },
        }}/>
        <Tab.Screen 
        name='MoreOption' 
        component={MoreOption} 
        options={{
          tabBarIcon: ({size}) => {
            return (
              <Image
                style={{ width:size, height: size }}
                source={require('./assets/moreOption.png')} />
            );
          },
        }}/>
        {/* try
        <TouchableOpacity onPress={()=> navigation.navigate('MoreOption')}>
          <Icon name={'menu'} type={'father'} color={'#696969'} size={30}></Icon>
          <Text> More </Text>
        </TouchableOpacity> */}
    </Tab.Navigator>
  )
}

const DrawerNavigator = () =>{
  return(
    <Drawer.Navigator drawerContent={ DrawerContent}>
    <Drawer.Screen name='HomeScreen' component={BottomNavigation}/>
    </Drawer.Navigator>
  )
}